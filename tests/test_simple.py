#  Copyright (c) 2020 Maksim Penkov
#  SPDX-License-Identifier: Apache-2.0

from spydi.context import DependencyContext, FactoryType
from spydi.factory import DependencyFactory


def multiplier(a, b):
    return a * b


def powerer(foo, bar):
    return foo ** bar


def test_simple_creation():
    ctx = DependencyContext()
    ctx.bind(2, to='a')
    ctx.bind(3, to='b')

    factory = DependencyFactory(ctx)
    answer = factory.create(multiplier)
    assert answer == 6


class Bar:
    def __init__(self, b):
        self.b = b


class Foo:
    def __init__(self, a: Bar, b: str, c=12):
        self.foo = (a, b, c)


def test_simple_resolve():
    ctx = DependencyContext()
    ctx.bind(2, to='a')
    ctx.bind(3, to='b')

    ctx.bind_factory(multiplier, to='foo', factory_type=FactoryType.SIMPLE)
    ctx.bind(2, to='bar')

    assert DependencyFactory(ctx).create(powerer) == 36


def test_class_resolve():
    ctx = DependencyContext()
    ctx.bind('foo', to='b')

    foo = DependencyFactory(ctx).create(Foo)

    bar, *rest = foo.foo

    assert bar.b == 'foo'
    assert rest == ['foo', 12]


class Baz:
    def __init__(self, bar: Bar, foo: Foo):
        self.bar = bar
        self.foo = foo


def test_reuse_objects():
    ctx = DependencyContext()
    ctx.bind('test', to='b')

    factory = DependencyFactory(ctx)
    baz = factory.create(Baz)

    assert baz.bar is baz.foo.foo[0]
    assert factory.obtain(Bar) is baz.bar
