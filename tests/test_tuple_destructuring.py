import typing

from spydi import context, factory
from spydi.tuple_destructuring import TupleDestructuringFactory


class BarConfig(typing.NamedTuple):
    a: str


class FooConfig(typing.NamedTuple):
    bar_conf: BarConfig
    b: int


class Config(typing.NamedTuple):
    foo_conf: FooConfig


def with_foo(foo_conf: FooConfig):
    return foo_conf.b


def with_bar(bar_conf: BarConfig):
    return bar_conf.a


def bar_through_subcontext(ctx: context.DependencyContext):
    return factory.DependencyFactory(ctx).create(with_bar)


def test_tuple_destructuring():
    ctx = context.DependencyContext()
    ctx.add_factory(TupleDestructuringFactory(Config))
    ctx.bind(
        Config(
            foo_conf=FooConfig(
                bar_conf=BarConfig(
                    a='foo',
                ),
                b=42,
            )
        ),
        to=Config,
    )

    assert factory.DependencyFactory(ctx).create(with_foo) == 42
    assert factory.DependencyFactory(ctx).create(with_bar) == 'foo'


def test_tuple_destructuring_persist_subcontext():
    ctx = context.DependencyContext()
    ctx.add_factory(TupleDestructuringFactory(Config))
    ctx.bind(
        Config(
            foo_conf=FooConfig(
                bar_conf=BarConfig(
                    a='foo',
                ),
                b=42,
            )
        ),
        to=Config,
    )

    assert factory.DependencyFactory(ctx).create(bar_through_subcontext) == 'foo'
