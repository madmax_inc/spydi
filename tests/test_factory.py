#  Copyright (c) 2020 Maksim Penkov
#  SPDX-License-Identifier: Apache-2.0

import asyncio
import contextlib
import inspect
import typing

import pytest

from spydi.context import (DependencyContext, FactoryType, BindingScope)
from spydi.factory import DependencyFactory


class FooConfig(typing.NamedTuple):
    foo_host: str


class CommonConfig(typing.NamedTuple):
    foo_config: FooConfig
    port: int


class FooUser:
    def __init__(self, foo_conf: FooConfig, bar=''):
        self.foo_conf = foo_conf
        self._bar = bar

    def bar(self):
        return self.foo_conf.foo_host + self._bar


def make_conf() -> CommonConfig:
    return CommonConfig(
        foo_config=FooConfig(
            foo_host='foo-host',
        ),
        port=12345,
    )


def make_foo_conf(common: CommonConfig) -> FooConfig:
    return common.foo_config


async def async_foo_conf(common: CommonConfig) -> FooConfig:
    await asyncio.sleep(0.1)
    return make_foo_conf(common)


def test_create_with_factory():
    ctx = DependencyContext()
    ctx.bind_factory(make_conf, factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(make_foo_conf, factory_type=FactoryType.SIMPLE)

    user = DependencyFactory(ctx).create(FooUser)

    assert user.bar() == 'foo-host'


def test_create_context_factory():
    ctx = DependencyContext()
    ctx.bind_factory(make_conf, factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(make_foo_conf, factory_type=FactoryType.SIMPLE)

    @contextlib.contextmanager
    def make_bar(common_conf: CommonConfig, finalizer):
        yield str(common_conf.port)
        finalizer()

    class Counter:
        def __init__(self):
            self.i = 0

    counter = Counter()

    def finalizer_maker(cnt):
        def finalizer():
            cnt.i += 1

        return finalizer

    ctx.bind_factory(make_bar, to='bar', factory_type=FactoryType.CONTEXT)
    ctx.bind(finalizer_maker(counter), to='finalizer')

    with DependencyFactory(ctx).create_ctx(FooUser) as user:
        assert user.bar() == 'foo-host12345'
        assert counter.i == 0

    assert counter.i == 1


@pytest.mark.asyncio()
async def test_create_with_async_factory():
    ctx = DependencyContext()
    ctx.bind_factory(make_conf, factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(async_foo_conf, factory_type=FactoryType.SIMPLE)

    user = DependencyFactory(ctx).create(FooUser)

    assert inspect.isawaitable(user)

    user = await user

    assert user.bar() == 'foo-host'


@pytest.mark.asyncio()
async def test_create_async_factory_reuse():
    ctx = DependencyContext()
    ctx.bind_factory(make_conf, factory_type=FactoryType.SIMPLE)
    ctx.bind_factory(async_foo_conf, factory_type=FactoryType.SIMPLE, scope=BindingScope.SINGLETON)

    def double_user(foo_user: FooUser, foo_conf: FooConfig):
        return foo_user.bar() + foo_conf.foo_host

    user = DependencyFactory(ctx).create(double_user)

    assert inspect.isawaitable(user)

    user = await user

    assert user == 'foo-host' * 2


@pytest.mark.asyncio()
async def test_create_async_context_factory():
    @contextlib.asynccontextmanager
    async def async_conf_maker(finalizer) -> CommonConfig:
        yield make_conf()
        await asyncio.sleep(0.2)
        await finalizer()

    class Counter:
        def __init__(self):
            self.i = 0

    counter = Counter()

    def finalizer_maker(cnt):
        async def finalizer():
            cnt.i += 1

        return finalizer

    ctx = DependencyContext()
    ctx.bind(finalizer_maker(counter), to='finalizer')
    ctx.bind_factory(async_conf_maker, factory_type=FactoryType.CONTEXT)
    ctx.bind_factory(async_foo_conf, factory_type=FactoryType.SIMPLE, scope=BindingScope.SINGLETON)

    async with DependencyFactory(ctx).create_ctx_async(
        FooUser
    ) as user:
        assert user.bar() == 'foo-host'
        assert counter.i == 0

    assert counter.i == 1
