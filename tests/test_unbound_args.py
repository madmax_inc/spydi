#  Copyright (c) 2020 Maksim Penkov
#  SPDX-License-Identifier: Apache-2.0

import typing

import pytest

from spydi.context import DependencyContext
from spydi.factory import DependencyFactory, FactoryType, ConstructionException


class Bar(typing.NamedTuple):
    a: str
    b: str


class Baz(typing.NamedTuple):
    c: Bar
    d: str


class FooBarBaz(typing.NamedTuple):
    strs: str


def make_d(bar: Bar, foobarbaz: FooBarBaz):
    return ''.join(
        [
            bar.a,
            bar.b,
            foobarbaz.strs,
        ]
    )


class Foo:
    def __init__(self, baz: Baz):
        self.d = baz.d


def test_unbound_args():
    ctx = DependencyContext()
    ctx.bind('a', to='a')
    ctx.bind('b', to='b')
    ctx.bind_factory(make_d, to='d', factory_type=FactoryType.SIMPLE)

    factory = DependencyFactory(ctx)

    with pytest.raises(ConstructionException) as ex:
        factory.create(Foo)
    trace = ex.value.trace()
    assert 'Argument strs is unconstructible, because: Argument is unbound' in trace
