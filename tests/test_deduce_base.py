#  Copyright (c) 2020 Maksim Penkov
#  SPDX-License-Identifier: Apache-2.0

import abc

from spydi.context import DependencyContext
from spydi.factory import DependencyFactory


class IFoo(abc.ABC):
    @abc.abstractmethod
    def bar(self):
        raise NotImplementedError


class Foo(IFoo):
    def bar(self):
        return 'bar'


class Bar:
    def bar(self):
        return 'baz'


def adder(bar, foo: IFoo):
    return bar.bar() + foo.bar()


def test_deduce_base():
    ctx = DependencyContext()
    ctx.bind(Bar, to='bar')
    ctx.bind(Foo)

    result = DependencyFactory(ctx).create(adder)
    assert result == 'bazbar'
