import pytest

from spydi.context import DependencyContext
from spydi.factory import DependencyFactory, ConstructionException


def make_bar(a, c):
    return a + c


def make_foo(b, ctx: DependencyContext):
    ctx.bind('c', to='c')
    return b + DependencyFactory(ctx).create(make_bar)


def test_sub_context():
    ctx = DependencyContext()
    ctx.bind('a', to='a')
    ctx.bind('b', to='b')

    assert DependencyFactory(ctx).create(make_foo) == 'bac'
    with pytest.raises(ConstructionException):
        DependencyFactory(ctx).create(make_bar)
